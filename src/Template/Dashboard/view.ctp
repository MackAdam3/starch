<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $pageTitle ?></title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('fonts.css') ?>
    <link href="http://fonts.googleapis.com/css?family=Oswald:700|Droid+Serif:400,700italic" rel="stylesheet" type="text/css" />
</head>
<body>
    <nav id="primary-menu">
        <ul>
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Projects</a></li>
            <li><a href="#">Users</a></li>
            <li><a href="#">Tasks</a></li>
        </ul>
    </nav>
</body>
</html>